import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const movies = [
        {id: 11, name: 'Avengers', medium: 'DVD'},
        {id: 12, name: 'Thor', medium: 'DVD'},
        {id: 13, name: 'Iron Man', medium: 'BD'},
        {id: 14, name: 'Hulk', medium: 'BR'},
        {id: 15, name: 'Ant Man', medium: 'BD'}
    ];
    return {movies};
  }
}
