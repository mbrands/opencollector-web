enum Medium {
  DVD = 'DVD',
  BD = 'Blu-ray'
}

export class Movie {
    id: number;
    name: string;
    medium: Medium;
  }
